package com.example.tutorial1.Utilities;

public interface BackPressedListener {
    void onBackPressed();
}
