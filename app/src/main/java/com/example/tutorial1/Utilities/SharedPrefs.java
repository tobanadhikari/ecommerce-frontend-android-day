package com.example.tutorial1.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

   private static final String APP_SHARED = "SharedPrefs";

   private SharedPreferences appSharedPreferences;
   private SharedPreferences.Editor appEditor;

    public SharedPrefs(Context context) {
        this.appSharedPreferences = context.getSharedPreferences(APP_SHARED,Context.MODE_PRIVATE);
        this.appEditor = appSharedPreferences.edit();
    }

    public String getString(String stringKeyValue){
        return appSharedPreferences.getString(stringKeyValue,"");
    }


    public void setString(String stringKeyValue, String _stringValue){
        appEditor.putString(stringKeyValue,_stringValue).commit();
    }


}
