package com.example.tutorial1.Utilities;

import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.Profile.ProfileModel;

import java.util.List;

public interface ProfileParser {
    public void processFinish(ProfileModel profile);

}