package com.example.tutorial1.Utilities;

import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.Products.Product;

import java.util.List;

public interface CartParser {
    void processFinish(List<CartItem> cartItemList);

}