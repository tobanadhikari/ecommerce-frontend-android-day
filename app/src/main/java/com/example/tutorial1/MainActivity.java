package com.example.tutorial1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.tutorial1.Adapter.ViewPagerAdapter;
import com.example.tutorial1.CartItem.CartItemActivity;
import com.example.tutorial1.Fragments.ProductListFragment;
import com.example.tutorial1.Fragments.SecondFragment;
import com.example.tutorial1.Fragments.CartListFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import static android.widget.Toast.LENGTH_SHORT;


public class MainActivity extends AppCompatActivity {

    static int x = 0;

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;


    ViewPagerAdapter viewPagerAdapter;

    ProductListFragment firstFragment;
    SecondFragment secondFragment;
    CartListFragment cartListFragment;

    DrawerLayout drawerLayout;
    NavigationView navigationView;

    Button go_to_cart_btn;

    SharedPreferences sharedPreferences;
    Context context;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        go_to_cart_btn = (Button) findViewById(R.id.go_to_cart_btn);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hamburger);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        firstFragment = new ProductListFragment();
        secondFragment = new SecondFragment();
        cartListFragment = new CartListFragment();

        viewPagerAdapter.addFregments(firstFragment, "First Tab");
        viewPagerAdapter.addFregments(secondFragment, "Second Tab");
        viewPagerAdapter.addFregments(cartListFragment, "Third Tab");


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        navigationView = findViewById(R.id.naviView);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                switch (menuItem.getItemId()) {

                    case R.id.first_tab:
                        menuItem.setChecked(true);
                        displaymessage("First Tab");
                        drawerLayout.closeDrawers();
                        viewPager.setCurrentItem(0, true);
                        x = 0;
                        return true;

                    case R.id.second_tab:
                        menuItem.setChecked(true);
                        displaymessage("Second Tab");
                        drawerLayout.closeDrawers();
                        viewPager.setCurrentItem(1, true);
                        x = 1;
                        return true;

                    case R.id.third_tab:
                        menuItem.setChecked(true);
                        displaymessage("Third Tab");
                        drawerLayout.closeDrawers();
                        viewPager.setCurrentItem(2, true);
                        x = 2;
                        return true;
                }
                return false;
            }


        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.go_to_cart_btn:
                Intent intent = new Intent(MainActivity.this, CartItemActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displaymessage(String message) {
        Toast.makeText(this, message, LENGTH_SHORT).show();
    }
}



