package com.example.tutorial1.CartItem;

import android.content.Context;
import android.os.AsyncTask;

import com.example.tutorial1.Products.Product;
import com.example.tutorial1.Utilities.AllParser;
import com.example.tutorial1.Utilities.CartParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.tutorial1.Utilities.Contants.GET_ALL_PRODUCT_LIST;
import static com.example.tutorial1.Utilities.Contants.GET_CUSTUMERS_CART_ITEM;
import static com.example.tutorial1.Utilities.Contants.TOKEN;

public class CartGetParser extends AsyncTask<String, Void, String> {

    Context context;
    CartParser cartParser;

    List<CartItem> cartItems = new ArrayList<>();

    public CartGetParser(Context context, CartParser cartParser) {
        this.context = context;
        this.cartParser = cartParser;
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url;
        HttpURLConnection httpURLConnection;

        try {
            url = new URL(GET_CUSTUMERS_CART_ITEM);
            httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("GET");
            //httpURLConnection.setRequestProperty("Content-Type", "application/json");

            httpURLConnection.setRequestProperty("Authorization", TOKEN);

            httpURLConnection.setDoInput(true);

            int resCode = httpURLConnection.getResponseCode();

            System.out.println("ResCode = " + resCode);

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String Line = "";
            StringBuilder responseoutput = new StringBuilder();

            while ((Line = reader.readLine()) != null) {
                responseoutput.append(Line);
            }
            System.out.println(("Response = " + responseoutput.toString()));

            return responseoutput.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("Response Cart Item Server " + s);

        try {
            JSONObject mainJjsonObject = new JSONObject(s);

            int totalPrice = mainJjsonObject.getInt("totalPrice");

            System.out.println("checkTotPric " +totalPrice);


            JSONArray jsonArray = mainJjsonObject.getJSONArray("cartItemList");
            for(int i=0;i<jsonArray.length();i++){
                JSONObject childJsonObject = jsonArray.getJSONObject(i);

                CartItem cartItem = new CartItem();

                cartItem.setId(childJsonObject.getInt("id"));

                JSONObject prdNameJson =  childJsonObject.getJSONObject("product");

                cartItem.setProduct(prdNameJson.getString("name"));


                cartItem.setQuantity(childJsonObject.getInt("quantity"));

                System.out.println("PrdQty " + childJsonObject.getInt("quantity"));

                cartItems.add(cartItem);
                cartParser.processFinish(cartItems);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
