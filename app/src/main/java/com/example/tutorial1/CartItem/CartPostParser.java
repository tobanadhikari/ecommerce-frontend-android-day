package com.example.tutorial1.CartItem;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.tutorial1.Utilities.Contants.POST_CUSTOMERS_CART_ITEM;
import static com.example.tutorial1.Utilities.Contants.TOKEN;

public class CartPostParser extends AsyncTask<String, Void, String> {
    Context mContext;
    CartItem cartItem;

    String productId;


    public CartPostParser(Context mContext, CartItem item, String prdId) {
        this.mContext = mContext;
        this.cartItem = item;
        this.productId = prdId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... voids) {

        URL url;
        HttpURLConnection connection = null;
        int rescode = -1;
        InputStream in = null;
        String json = null;

        try {

            url = new URL(POST_CUSTOMERS_CART_ITEM);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            //connection.setConnectTimeout(10000);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", TOKEN);

            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            // connection.setRequestProperty("Authorization", TOKEN);
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());

            final JSONObject params = new JSONObject();

            JSONObject pIdJson = new JSONObject();

            try {

                pIdJson.put("id", productId);

                params.put("product", pIdJson);
                params.put("quantity", cartItem.getQuantity());
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("CheckReq " + params);

            dataOutputStream.writeBytes(params.toString());

            dataOutputStream.flush();
            dataOutputStream.close();

            connection.connect();

            rescode = connection.getResponseCode();
            Log.e("resCode ", String.valueOf(rescode));


            in = connection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

            String line = "";
            final StringBuilder responseOutput = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                responseOutput.append(line);
            }

            bufferedReader.close();
            json = responseOutput.toString();
            System.out.println("Check " + responseOutput.toString());


            return json;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("CartResponse " + s);
    }
}
