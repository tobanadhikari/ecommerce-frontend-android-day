package com.example.tutorial1.CartItem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorial1.DetailsActivity;
import com.example.tutorial1.Products.Product;
import com.example.tutorial1.R;
import com.example.tutorial1.Utilities.BackPressedListener;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyHolder> {

    Context context;
    List<CartItem> cartItemArrayList;

    public static List<CartItem> choosenCartArryList = new ArrayList<>();

    String  prdName;
    int prdQuantity;

    public CartAdapter(Context context, List<CartItem> cartList) {
        this.context = context;
        this.cartItemArrayList = cartList;
    }

    @NonNull
    @Override
    public CartAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_cart, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CartAdapter.MyHolder holder, final int position) {
        final CartItem currentItem = cartItemArrayList.get(position);
        System.out.println("Detail= " + currentItem.getQuantity());

        System.out.println("DetailFrom= " + currentItem.getProduct());

        holder.choose_radio.setText(currentItem.getProduct());
        holder.product_quantity_cart.setText(Integer.toString(currentItem.getQuantity()));

        prdQuantity = currentItem.getQuantity();
        //queImg = currentItem.getImage();
        prdName = currentItem.getProduct();

        //new SendHttpRequestTask(holder.question_image).execute("http://192.168.0.108:3000/api/question/images/" + queImg);

        holder.choose_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                // Check which radiobutton was pressed
                if (checked){
                    holder.choose_radio.setChecked(true);
                    String chosValue = String.valueOf(holder.choose_radio.getText());

                    CartItem cartItem = new CartItem();
                    cartItem.setProduct(chosValue);
                    cartItem.setQuantity(currentItem.getQuantity());
                    cartItem.setAnswered(true);

                    choosenCartArryList.add(cartItem);


                    System.out.println("chossQuan " + currentItem.getQuantity());
                }
                else{
                    holder.choose_radio.setChecked(false);
                }
            }
        });


        holder.main_section_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*for converting imgview to bitmap*/
              /*  holder.question_image.buildDrawingCache();
                Bitmap bitmap = holder.question_image.getDrawingCache();*/

                Intent detailIntent = new Intent(context, DetailCartActivity.class);
                CartItem clickedItem = cartItemArrayList.get(position);

                detailIntent.putExtra("product_name_cart", clickedItem.getProduct());
                detailIntent.putExtra("product_quantity_cart", clickedItem.getQuantity());

                context.startActivity(detailIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItemArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView product_quantity_cart;
        LinearLayout main_section_cart;
        RadioButton choose_radio;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            product_quantity_cart = (TextView) itemView.findViewById(R.id.product_quantity_cart);

            main_section_cart = (LinearLayout) itemView.findViewById(R.id.main_section_cart);
            choose_radio = (RadioButton) itemView.findViewById(R.id.choose_radio);
        }
    }
}
