package com.example.tutorial1.CartItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tutorial1.PlaceOrder.PlaceOrderActivity;
import com.example.tutorial1.R;
import com.example.tutorial1.Utilities.CartParser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CartItemActivity extends AppCompatActivity {

    RecyclerView recyclerView_cart;
    List<CartItem> cartItemArrayList;
    CartAdapter cartAdapter;
    Button check_out;

    List<CartItem> allSelectionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_item);

        recyclerView_cart = (RecyclerView) findViewById(R.id.recyclerView_cart);
        recyclerView_cart.setLayoutManager(new LinearLayoutManager(CartItemActivity.this));
        check_out = (Button) findViewById(R.id.check_out);

        new CartGetParser(CartItemActivity.this, new CartParser() {
            @Override
            public void processFinish(List<CartItem> cartItemList) {

                cartItemArrayList = cartItemList;
                System.out.println("ListInMain " + cartItemArrayList.size());

                if (cartItemArrayList.size() != 0) {

                    cartAdapter = new CartAdapter(CartItemActivity.this, cartItemArrayList);
                    recyclerView_cart.setAdapter(cartAdapter);
                    cartAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(CartItemActivity.this, "No Data", Toast.LENGTH_LONG).show();
                }
            }
        }).execute();

        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i = 0; i < CartAdapter.choosenCartArryList.size(); i++) {
                    if ( CartAdapter.choosenCartArryList.get(i).isAnswered()) {

                        CartItem cartItem = CartAdapter.choosenCartArryList.get(i);

                        String product_name = cartItem.getProduct();
                        String product_quantity = String.valueOf(cartItem.getQuantity());

                        CartItem cartModel = new CartItem();
                        cartModel.setProduct(product_name);
                        cartModel.setQuantity(Integer.parseInt(product_quantity));

                        System.out.println("InQuanItemQu "+product_quantity);

                        allSelectionList.add(cartModel);

                    }
                }

                System.out.println("allSelectionListInACT " + allSelectionList.size());
                Intent intent = new Intent(CartItemActivity.this, PlaceOrderActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("allSelectionList", (Serializable) allSelectionList);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
