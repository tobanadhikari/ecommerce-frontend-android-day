package com.example.tutorial1.PlaceOrder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.CartItem.DetailCartActivity;
import com.example.tutorial1.R;

import java.util.ArrayList;
import java.util.List;

public class PlaceOrderListAdapter extends RecyclerView.Adapter<PlaceOrderListAdapter.MyHolder> {

    Context context;
    List<CartItem> cartItemArrayList;

    public static List<CartItem> choosenCartArryList = new ArrayList<>();

    String  prdName;
    int prdQuantity;

    public PlaceOrderListAdapter(Context context, List<CartItem> cartList) {
        this.context = context;
        this.cartItemArrayList = cartList;
    }

    @NonNull
    @Override
    public PlaceOrderListAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_list_order_place, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final PlaceOrderListAdapter.MyHolder holder, final int position) {
        CartItem currentItem = cartItemArrayList.get(position);
        System.out.println("Detail= " + currentItem.getQuantity());

        System.out.println("DetailFrom= " + currentItem.getProduct());

        holder.product_name_place_order.setText(currentItem.getProduct());
        holder.product_quantity_place_order.setText(Integer.toString(currentItem.getQuantity()));

        prdQuantity = currentItem.getQuantity();
        //queImg = currentItem.getImage();
        prdName = currentItem.getProduct();

        //new SendHttpRequestTask(holder.question_image).execute("http://192.168.0.108:3000/api/question/images/" + queImg);
    }

    @Override
    public int getItemCount() {
        return cartItemArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView product_quantity_place_order,product_name_place_order;
        LinearLayout main_section_place_order;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            product_quantity_place_order = (TextView) itemView.findViewById(R.id.product_quantity_place_order);
            product_name_place_order = (TextView) itemView.findViewById(R.id.product_name_place_order);
            main_section_place_order = (LinearLayout) itemView.findViewById(R.id.main_section_place_order);
        }
    }
}
