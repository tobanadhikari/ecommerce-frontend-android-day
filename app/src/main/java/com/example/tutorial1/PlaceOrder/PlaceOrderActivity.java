package com.example.tutorial1.PlaceOrder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;

import com.example.tutorial1.CartItem.CartAdapter;
import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.CartItem.CartItemActivity;
import com.example.tutorial1.Profile.ProfileGetParser;
import com.example.tutorial1.Profile.ProfileModel;
import com.example.tutorial1.R;

import com.example.tutorial1.Utilities.ProfileParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlaceOrderActivity extends AppCompatActivity {

    ProfileModel profileModel;
    String userFullname, userEmail, userAddress, userContact, userName;

    TextView full_name_text_usr, address_text_usr, contact_text_usr, email_text_usr;
    Toolbar toolbar_place_order;

    List<CartItem> getChooseCartArrayList = new ArrayList<>();

    RecyclerView recyclerview_place_order;
    PlaceOrderListAdapter placeOrderListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        full_name_text_usr = (TextView) findViewById(R.id.full_name_text_usr);
        address_text_usr = (TextView) findViewById(R.id.address_text_usr);
        contact_text_usr = (TextView) findViewById(R.id.contact_text_usr);
        email_text_usr = (TextView) findViewById(R.id.email_text_usr);

        toolbar_place_order = (Toolbar) findViewById(R.id.toolbar_place_order);
        setSupportActionBar(toolbar_place_order);

        recyclerview_place_order = (RecyclerView) findViewById(R.id.recyclerview_place_order);
        recyclerview_place_order.setLayoutManager(new LinearLayoutManager(PlaceOrderActivity.this));

        Bundle bundleObj = getIntent().getExtras();
        if (bundleObj != null) {

            getChooseCartArrayList = (ArrayList<CartItem>) bundleObj.getSerializable("allSelectionList");

            System.out.println("getChooseCartArrayList " + getChooseCartArrayList.size());
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                placeOrderListAdapter = new PlaceOrderListAdapter(PlaceOrderActivity.this, getChooseCartArrayList);
                recyclerview_place_order.setAdapter(placeOrderListAdapter);
                placeOrderListAdapter.notifyDataSetChanged();

            }
        });


        toolbar_place_order.setNavigationIcon(R.drawable.back_arrow);
        toolbar_place_order.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PlaceOrderActivity.this, CartItemActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                CartAdapter.choosenCartArryList.removeAll( CartAdapter.choosenCartArryList);
                System.out.println("ClickedBackerr " + CartAdapter.choosenCartArryList.size());
                startActivity(intent);
                finish();
            }
        });


        new ProfileGetParser(PlaceOrderActivity.this, new ProfileParser() {
            @Override
            public void processFinish(ProfileModel profile) {
                profileModel = profile;
                userFullname = profileModel.getName();
                userAddress = profileModel.getAddress();
                userName = profileModel.getUsername();
                userContact = profileModel.getContact();
                userEmail = profileModel.getEmail();

                System.out.println("userNameOrder" + userFullname);

                full_name_text_usr.setText(userFullname);
                email_text_usr.setText(userEmail);
                contact_text_usr.setText(userContact);
                address_text_usr.setText(userAddress);


            }
        }).execute();
    }

}
