package com.example.tutorial1.Profile;

import android.content.Context;
import android.os.AsyncTask;

import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.Utilities.CartParser;
import com.example.tutorial1.Utilities.ProfileParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.tutorial1.Utilities.Contants.GET_CUSTOTMER_PROFILE;
import static com.example.tutorial1.Utilities.Contants.GET_CUSTUMERS_CART_ITEM;
import static com.example.tutorial1.Utilities.Contants.TOKEN;

public class ProfileGetParser extends AsyncTask<String, Void, String> {

    Context context;
    ProfileParser profileParser;


    public ProfileGetParser(Context context, ProfileParser profileParser) {
        this.context = context;
        this.profileParser = profileParser;
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url;
        HttpURLConnection httpURLConnection;

        try {
            url = new URL(GET_CUSTOTMER_PROFILE);
            httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            httpURLConnection.setRequestProperty("Authorization", TOKEN);

            httpURLConnection.setDoInput(true);

            int resCode = httpURLConnection.getResponseCode();

            System.out.println("ResCode = " + resCode);

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String Line = "";
            StringBuilder responseoutput = new StringBuilder();

            while ((Line = reader.readLine()) != null) {
                responseoutput.append(Line);
            }
            System.out.println(("Response = " + responseoutput.toString()));

            return responseoutput.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("Response Profile Server " + s);

        try {
            JSONObject mainJjsonObject = new JSONObject(s);

            ProfileModel profileModel = new ProfileModel();
            profileModel.setId(mainJjsonObject.getInt("id"));
            profileModel.setUsername(mainJjsonObject.getString("username"));
            profileModel.setAddress(mainJjsonObject.getString("address"));
            profileModel.setEmail(mainJjsonObject.getString("email"));
            profileModel.setContact(mainJjsonObject.getString("contact"));
            profileModel.setName(mainJjsonObject.getString("name"));


            System.out.println("CheckProfile " + mainJjsonObject.getString("username"));

            profileParser.processFinish(profileModel);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }
}
