package com.example.tutorial1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.CartItem.CartItemActivity;
import com.example.tutorial1.CartItem.CartPostParser;
import com.google.android.material.snackbar.Snackbar;


public class DetailsActivity extends AppCompatActivity {

    String prdName,prdId;
    String prdQuantity = "1";
    TextView product_name, product_quantity;
    Button btn_add_to_cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        prdName = intent.getStringExtra("product_name");

        prdId = intent.getStringExtra("product_id");
        System.out.println("InDetailPrdId "+ prdId);

        product_name = (TextView) findViewById(R.id.prd_name_text);
        product_quantity = (TextView) findViewById(R.id.prd_qunty);
        btn_add_to_cart = (Button) findViewById(R.id.brn_add_to_cart);
        //question_image = (CircleImageView) findViewById(R.id.question_image);

        product_name.setText(prdName);
        product_quantity.setText(prdQuantity);

        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("question_image");
        //question_image.setImageBitmap(bitmap);

        btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                CartItem cartItem = new CartItem();
                cartItem.setProduct(prdName);
                cartItem.setQuantity(Integer.parseInt(prdQuantity));

                new CartPostParser(DetailsActivity.this, cartItem, prdId).execute();

                Snackbar snackbar = Snackbar.make(view, "Add To Cart", Snackbar.LENGTH_LONG);

                snackbar.setAction("Go To Cart", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbarsuccess = Snackbar.make(view, "Successfully added to cart", Snackbar.LENGTH_LONG);
                        snackbarsuccess.show();

                        Intent intentCart = new Intent(DetailsActivity.this, CartItemActivity.class);
                        startActivity(intentCart);
                    }
                });
                snackbar.show();
            }
        });
    }
}