package com.example.tutorial1.Adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> tabTitles = new ArrayList<>();

    //for exchanging fragments and tab titles
    public void addFregments(Fragment fragment, String tabTitle) {

        fragmentList.add(fragment);
        tabTitles.add(tabTitle);
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /*get fragment item position*/
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    /*show total number of item*/
    @Override
    public int getCount() {
        return fragmentList.size();
    }

    /*for getting tabtitles*/
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}
