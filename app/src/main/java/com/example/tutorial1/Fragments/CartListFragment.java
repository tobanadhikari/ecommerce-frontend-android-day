package com.example.tutorial1.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.tutorial1.CartItem.CartAdapter;
import com.example.tutorial1.CartItem.CartGetParser;
import com.example.tutorial1.CartItem.CartItem;
import com.example.tutorial1.Products.Product;
import com.example.tutorial1.Products.ProductAdapter;
import com.example.tutorial1.Products.ProductParser;
import com.example.tutorial1.R;
import com.example.tutorial1.Utilities.AllParser;
import com.example.tutorial1.Utilities.CartParser;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartListFragment extends Fragment {



    public CartListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);


        return view;
    }

}
