package com.example.tutorial1.Fragments;


import android.app.Activity;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.tutorial1.Adapter.ImageAdapter;
import com.example.tutorial1.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    ViewPager viewPager;
    ImageAdapter imageAdapter;
    LinearLayout dots_container;
    int dots_count;
    ImageView[] imageViews;
    Activity activity;

    public SecondFragment() {
        // Required empty public constructor
    }
/*   */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.activity = activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "Must implement");
        } catch (NullPointerException ex) {
            throw new NullPointerException(activity.toString());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_second, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        dots_container = view.findViewById(R.id.dots_container);
        imageAdapter = new ImageAdapter(activity);
        dots_count = imageAdapter.getCount();
        viewPager.setAdapter(imageAdapter);
        imageViews = new ImageView[dots_count];
        for (int i = 0; i < dots_count; i++) {
            imageViews[i] = new ImageView(activity);
            imageViews[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.inactive_dots));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            dots_container.addView(imageViews[i], params);
        }

        imageViews[0].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dots));


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dots_count; i++) {
                    imageViews[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.inactive_dots));
                }
                imageViews[position].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dots));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 2000);
        return view;
    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            Activity activity = getActivity();

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (viewPager.getCurrentItem() == 0) {
                            viewPager.setCurrentItem(1);
                        } else if (viewPager.getCurrentItem() == 1) {
                            viewPager.setCurrentItem(2);
                        } else if (viewPager.getCurrentItem() == 2) {
                            viewPager.setCurrentItem(3);
                        } else if (viewPager.getCurrentItem() == 3) {
                            viewPager.setCurrentItem(4);
                        } else if (viewPager.getCurrentItem() == 4) {
                            viewPager.setCurrentItem(5);
                        } else if (viewPager.getCurrentItem() == 5) {
                            viewPager.setCurrentItem(6);
                        } else if (viewPager.getCurrentItem() == 6) {
                            viewPager.setCurrentItem(0);
                        }
                    }
                });
            }


        }
    }

}
