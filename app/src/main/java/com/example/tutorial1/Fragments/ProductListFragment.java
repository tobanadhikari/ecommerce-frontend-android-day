package com.example.tutorial1.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.tutorial1.Products.Product;
import com.example.tutorial1.Products.ProductAdapter;
import com.example.tutorial1.Products.ProductParser;
import com.example.tutorial1.R;
import com.example.tutorial1.Utilities.AllParser;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment {

    RecyclerView recyclerView;
    List<Product> productArrayList;
    ProductAdapter productAdapter;

    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_product);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new ProductParser(getActivity(), new AllParser() {
            @Override
            public void processFinish(List<Product> productList) {
                productArrayList = productList;
                System.out.println("ListInMain " + productArrayList.size());

                if (productArrayList.size() != 0) {
                    productAdapter = new ProductAdapter(getActivity(), productArrayList);
                    recyclerView.setAdapter(productAdapter);
                    productAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_LONG).show();
                }
            }
        }).execute();

        return view;
    }
}
